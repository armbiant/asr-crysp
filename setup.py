from setuptools import setup, find_packages

dependencies = [
    'ase',
    'gpaw',
    'numpy',
    'pytest',
    'pandoc','ipython','nbsphinx', # for compiling jupyter notebooks
    'sphinx','sphinx-rtd-theme','sphinx_autodoc_annotation', # sphinx requires
    # Well, we need htw-util, which might still change name.
    # Or "asr", which does not have a well-defined version.
    # We probably also need some other things.
]


setup(
    name='crysp',
    version='0.1',
    packages=find_packages(),
    python_requires='>=3.8',
    install_requires=dependencies,
    description='workflow code for relaxations and possibly other things',
)
