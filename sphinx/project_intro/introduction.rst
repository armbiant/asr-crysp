============
Introduction
============

The `asr-crysp` package utilized: `ASR`_, `ASE`_, `TaskBlaster`_, and
`GPAW`_ to perform high-throughput ab-initio calculations. The `asr-crysp`
package is tailored to addressing scientific questions regarding the
stability, reactivity, optical, and catalytic properties of bulk crystal
structures.

.. _ASR: https://gitlab.com/asr-dev/asr
.. _ASE: https://gitlab.com/ase/ase
.. _GPAW: https://gitlab.com/gpaw/gpaw
.. _TaskBlaster: https://gitlab.com/askhl/htwutil
.. _MyQueue: https://myqueue.readthedocs.io/en/latest/

Overview of ASR-CRYSP Package
=============================

The `asr-crysp` package contains the following structure outlined below. The
code is broken up into 3 main user-facing directories: Workflows, Tasks, and
Utils. Additionally, there is the post-processing & web panel directory
containing routines for analyzing results and viewing the results in web
panels.

1. workflows

    The "workflows" directory contains a set of pre-defined *tasks* that
    constitutes a *recipe* in `ASR` terminology which computes some materials'
    property. We utilize the `TaskBlaster` package to automate and track
    task/workflow dependencies and `MyQueue`_ to submit and manage the running
    jobs.

    Workflows outline all the high level, important scientific decisions made
    in a workflow. They should be easily readable and understandable. Not
    bogged down with details. The details are encoded into tasks which possess
    descriptive names.

2. tasks

    A "task" is a short and specific piece of code that performs a single task
    in a workflow. The *task* directory is a library of these simple,
    re-usable bits of code that are commonly used in workflows. These tasks
    should be as isolated as possible from other parts of the code and
    return something useful to the user.

    The key to writing a good task is to have them be well-defined, short, and
    digestible. It should not contain important decisions about the workflow.
    Nor should it hide decisions from the user.

3. utils

    This is currently somewhere to place functions/classes/bits of code that
    aren't exactly a task or a workflow but are important to provide the user
    with.

4. postprocess

    The *postprocess* directory contains leaf node tasks that analyze the
    results of the pre-defined workflows.


How to cite ASR-CRYSP
=====================

If you use `asr-crysp` in your research, please consider citing the following
work:

    <citations>

License
=======

The `asr-crysp` package is released under the GNU General Public Version 3
`License <https://fsf.org/>`_. Copyright (C) 2007 Free Software Foundation,
Inc. The terms of the license can be found in the main directory of this
software package under the LICENSE file.

About the Team
==============

Kristian Sommer Thygesen (P.I.) in the Computational Atomic-scale Materials
Design (CAMD) started `asr-crysp` in 2022, and is project lead.

**Developers**

1. Ask Hjorth Larsen

2.  Mark Kamper Svendsen

3. Tara Maria Boland

4. Julian Heske

Copyright Policy
================

`asr-crysp` uses a shared copyright model. Each contributor maintains
copyright over their contributions to `asr-crysp`. But, it is important
to note that these contributions are typically only changes to the 
repositories. Thus, the `asr-crysp` source code, in its entirety is not
the copyright of any single person or institution. Instead, it is the 
collective copyright of the entire `asr-crysp` Development Team.

With this in mind, the following banner should be used in any source 
code file to indicate the copyright and license terms::

  # Copyright (c) CAMD Development Team.
  # Distributed under the terms of the GNU License.

