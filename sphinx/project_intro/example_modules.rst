Overview
========

To get started using `asr-crysp`, various tutorials and examples have been created
using Jupyter Notebooks. These notebooks demonstrate the basic functionality of
`asr-crysp` to enable users to quickly learn how to use the various modules within
this package. These can be found under *asr-crysp/examples*.

Examples should show the user how to:

- run a WF on:

    - one structure
    - many structures

- write a:

    - workflow
    - task

- write post-processing

- deal with:

    - failed job
    - restart a job
    - delete a job

.. toctree::
   :maxdepth: 1

   examples/ExampleWorkflow
