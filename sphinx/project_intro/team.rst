================
Development Team
================

The `asr-crysp` package has a small development team.

Lead Maintainers
================

| **Ask Hjorth Larsen**   |askhl|  |0000-0001-5267-6852|
| Postdoctoral Researcher, Department of Physics
| Danmarks Tekniske Universitet, Denmark

.. |askhl| image:: ../images/gitlab.png
   :target: https://gitlab.com/askhl
   :width: 26
   :height: 26
   :alt: GitLab profile for askhl

.. |0000-0001-5267-6852| image:: ../images/orcid.jpg
   :target: https://orcid.org/0000-0001-5267-6852
   :width: 26
   :height: 26
   :alt: ORCID profile for 0000-0001-5267-6852

.. |Mark_Kamper| image:: ../images/gitlab.png
   :target: https://gitlab.com/Mark_Kamper
   :width: 26
   :height: 26
   :alt: GitLab profile for Mark_Kamper

.. |0000-0001-9718-849X| image:: ../images/orcid.jpg
   :target: https://orcid.org/0000-0001-9718-849X
   :width: 26
   :height: 26
   :alt: ORCID profile for 0000-0001-9718-849X

| **Tara M. Boland**   |tboland1|   |0000-0002-2587-5677|
| Postdoctoral Researcher, Department of Physics
| Danmarks Tekniske Universitet, Denmark

.. |tboland1| image:: ../images/gitlab.png
   :target: https://gitlab.com/tboland1
   :width: 26
   :height: 26
   :alt: GitLab profile for tboland1

.. |0000-0002-2587-5677| image:: ../images/orcid.jpg
   :target: https://orcid.org/0000-0002-2587-5677
   :width: 26
   :height: 26
   :alt: ORCID profile for 0000-0002-2587-5677

| **Mark Kamper Svendsen**   |Mark_Kamper|  |0000-0001-9718-849X|
| Postdoctoral Researcher, Department of Physics
| Danmarks Tekniske Universitet, Denmark

| **Julian Heske**   |jjheske|  |0000-0001-6503-9967|
| Postdoctoral Researcher, Department of Physics
| Danmarks Tekniske Universitet, Denmark

.. |jjheske| image:: ../images/gitlab.png
   :target: https://gitlab.com/jjheske
   :width: 26
   :height: 26
   :alt: GitLab profile for jjheske

.. |0000-0001-6503-9967| image:: ../images/orcid.jpg
   :target: https://orcid.org/0000-0001-6503-9967
   :width: 26
   :height: 26
   :alt: ORCID profile for 0000-0001-6503-9967

List of Developers (A-Z)
========================

