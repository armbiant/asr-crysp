=====================
Installing ASR-CRYSP
=====================

1. Download the repo from the green code icon or via GitLab's commandline
tool gh.

  * ``gh repo clone asr-dev/asr-crysp`` (gh must be installed)
  * ``git clone https://gitlab.com/asr-dev/asr-crysp.git``

2. Install asr-crysp in a clean environment using python=3.8. I suggest using
Anaconda3 to manage environments.

  * ``conda create --name crysp python=3.8``

3. Activate the `asr-crysp` environment and run the line below to install
`asr-crysp`. Must be in the directory where `asr-crysp` was downloaded to.

  * ``pip install -r requirements.txt``

4. After installation, `asr-crysp` needs to be added to your python path. This
can be done by running the first line below **OR** by adding the 2nd line to
your *.bashrc* file. This is only necessary if python cannot find the
package or the setup.py failed for some reason.

  * ``python setup.py develop`` or ``python setup.py install``
  * ``export PYTHONPATH="$HOME/path_to_package/asr-crysp:$PYTHONPATH"``


5. To run jupyter notebooks on various resources the ipykernel has to be
installed. Sometimes this isn't enough and you need explicitly add the kernel
to the list of environments.

  * Activate your environment ``conda activate crysp``
  * ``python -m ipykernel install --user --name crysp``

Special install instructions for CAMD's Niflheim supercomputer

1. Run install_gpaw.sh on Niflheim to set up the newest `ASR` workflow.
2. Add the `asr-crysp` to your venv's python path via:

    ``$ export PYTHONPATH="\<path-to-asr-crysp\>:$PYTHONPATH"``

Setting up dependencies
========================

1. Fill in some instructions here.


