# Welcome to asr-crysp
The ASR CRYstal Structure Property (`asr-crysp`) workflow repository is a 
high-throughput workflow project for predicting the properties of bulk
materials with the ultimate goal of characterizing the optical and catalytic
properties of materials.

Check out the doc pages [here](https://asr-dev.gitlab.io/asr-crysp/).


# Running the workflow
```
$ asr init
$ asr workflow totree.py
$ asr workflow path/to/workflow.py
```

# Building the sphinx documentation

1. Within the *sphinx* directory run
``` 
$ make csphinx
```

That is it. That command creates the docs directory with all necessary links 
and relevant html files to host on a webpage. The *docs/html* directory is 
typically what is hosted on a webpage. The docs directory can be deleted 
whenever you want, and you can re-execute the build commands to re-generate the 
documentation.

#### Important files for sphinx documentation
A list of files which control how and what is compiled as documentation.

1. The *sphinx/config.py* file controls what is compiled.
2. The *sphinx/index.rst* controls the first page the user will see. 
   1. This is the master file which is used to reference new sub-folders and 
      include other doc pages/notebooks. If you need to add to the 
      documentation in another directory make sure they get added to this file.
3. The *project_intro*, *examples*, and *images* directories. 
   1. These directories are entirely user-created. It just populates the 
      web-page with information about the package, dev team, installation, etc.