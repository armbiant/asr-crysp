from crysp.tasks.general import mpi, dummy_parallel_function


def test_parallel():
    with mpi() as parallel:
        five, ranksum = parallel.call(dummy_parallel_function, 2, b=3)

    print('results', five, ranksum)
    assert five == 5
    # Can we see how many cores we actually have and verify the ranksum?
