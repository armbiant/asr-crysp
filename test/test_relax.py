import pytest
from crysp.tasks.relax import niggli_normalize, run_optimization
from ase.build import bulk
from ase.utils import workdir
from ase.calculators.emt import EMT
from ase.calculators.calculator import kpts2kpts
from gpaw import GPAW
import numpy as np


def emt_energy(atoms):
    atoms = atoms.copy()
    atoms.calc = EMT()
    return atoms.get_potential_energy()


def emt_energy_equal(atoms1, atoms2, abs=1e-10):
    e1 = emt_energy(atoms1)
    e2 = emt_energy(atoms2)
    return e1 == pytest.approx(e2, abs=abs)


@pytest.fixture
def testdir(tmp_path):
    """Create and go to temporary directory."""
    with workdir(tmp_path):
        yield tmp_path


def test_niggli(testdir):
    orig_atoms = bulk('Si')
    orig_atoms.symbols[:] = 'Au'
    orig_atoms.rattle()

    unred_atoms = orig_atoms.copy()
    unred_atoms.cell[2] += unred_atoms.cell[0]

    red_atoms = niggli_normalize(unred_atoms)

    assert unred_atoms.cell.angles() != pytest.approx(60)
    assert red_atoms.cell.angles() == pytest.approx(60)
    assert red_atoms.cell.cellpar() == pytest.approx(
        orig_atoms.cell.cellpar())

    # It is difficult to fully verify that two structures are equivalent.
    # But if they have the same EMT energy, then they are "probably" equal.
    assert emt_energy_equal(orig_atoms, red_atoms)


def test_runoptimization(testdir):
    # check that the correct fmax, smax, and restart functionality works
    # the correct output files are written
    calculator = {'mode': {'name': 'pw', 'ecut': 400}, 'kpts': {'density': 2},
                  'convergence': {'forces': 5e-2},
                  'txt': 'relax.txt'}
    smax, fmax = 0.005, 0.05
    atoms = bulk('Si')
    calculator['kpts'] = kpts2kpts(calculator['kpts'], atoms) if isinstance(
        calculator['kpts'], dict) else calculator['kpts']  # kpts obj

    # init gpaw calc & attach to atoms
    calc = GPAW(**calculator)
    atoms.calc = calc

    # run optimization
    atoms, traj = run_optimization(atoms, fmax=fmax, smax=smax,
                                   identifier='restart.json')
    # check that fmax and smax are below tols
    f = np.linalg.norm(atoms.calc.get_forces(), axis=1).max()**0.5
    s = abs(atoms.calc.get_stress() * np.array([1, 1, 1, 1, 1, 1])).max()
    assert f <= fmax
    assert s <= smax
    # TODO test restart functionality


def test_relax3d(testdir):
    from crysp.tasks.relax import relax3d, magnetize
    atoms = bulk('Si')

    # test magnetize routine
    atoms = magnetize(atoms)
    assert atoms.get_initial_magnetic_moments().all() == 1

    # Ensure mag to non-mag relax routine functions
    calculator = {'mode': {'name': 'pw', 'ecut': 200}, 'xc': 'PBE',
                  'kpts': {'density': 1},
                  'convergence': {'forces': 5e-1}, 'smax': 5e-2,
                  'txt': 'relax.txt'
                  }
    relax = relax3d(atoms=atoms, calculator=calculator)

    # read results from relaxation run
    from ase.io import read
    traj = read(relax['traj_path'])
    pe = traj.get_potential_energy()

    assert pe == pytest.approx(-10.1598173, 1e-3)
    assert relax['traj_path'].name == 'relax-nm.traj'

