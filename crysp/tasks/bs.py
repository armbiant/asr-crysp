"""
The tasks associated with band structure calculations using GPAW.
Intended use for 3D structures.
"""
import typing
from pathlib import Path

from crysp.tasks.general import mpi

from gpaw import GPAW


def _bs_calculate(gs_gpw_file_path: Path, bs_settings: dict) -> typing.Union[Path]:
    """
    Task to perform a bandstructure calculation
    :param gs_gpw_file_path: Path object to the *.gpw file
    :param bs_settings: dict containing the desired bandpath 
    and density of k-points along the bandpath. 
    {'bandpath': str, 'density': float}
    The bandpath can be specified as a set of high-symmetry points. If
    nothing is specified, the default bandpath for the cell as determined
    by atoms.cell.bandpath will be used.
    :return bs_file_path: path object for the bandstructure object
    """
    gscalc = GPAW(gs_gpw_file_path)
    atoms = gscalc.get_atoms()
    bandpath = atoms.cell.bandpath(bs_settings['bandpath'], bs_settings['density'])
    calc = gscalc.fixed_density(
        kpts=bandpath.kpts, symmetry='off', txt='bandstructure.txt')
    bs = calc.band_structure()
    calc.write('bandstructure.gpw')
    bs_file_path=Path('bandstructure.json')
    bs.write(bs_file_path)
    return bs_file_path


def bs_calculate(gs_gpw_file_path: Path, bs_settings: dict) -> typing.Union[Path]:
    """
    Helper function which invokes the _bs_calculate task
    inside a parallel context
    :param gs_gpw_file_path: Path object to the *.gpw file
    :param bs_settings: dict containing the desired bandpath 
    and density of k-points along the bandpath. 
    {'bandpath': str, 'density': float}
    The bandpath can be specified as a set of high-symmetry points. If
    nothing is specified, the default bandpath for the cell as determined
    by atoms.cell.bandpath will be used.
    :return bs_file_path: path object for the bandstructure object
    """
    with mpi() as parallel:
        bs_file_path = parallel.call(_bs_calculate,
                                     gs_gpw_file_path=gs_gpw_file_path,
                                     bs_settings=bs_settings)
    return bs_file_path
