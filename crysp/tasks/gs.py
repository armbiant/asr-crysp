"""
The tasks associated with ground state calculations using GPAW.
Intended use for 3D structures.
"""
import typing

from pathlib import Path
from ase import Atoms

from crysp.tasks.general import gpaw


def gs_calculate(atoms: Atoms, calculator: dict) -> typing.Union[Atoms, Path]:
    """
    Task to perform an electronic groundstate calculation

    :param atoms: ASE atoms object coming from the structure
    relaxation
    :param calculator: A dictionary containing the calculator
    input parameters
    :return: Path object for the ground state file gs.gpw
    """
    with gpaw(calculator) as atoms.calc:
        atoms.get_potential_energy()
        path = Path('gs.gpw')
        atoms.calc.write(path)
    return path
