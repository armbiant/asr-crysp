"""
The tasks associated with the relaxation of atomic structures using GPAW.
Intended use for 3D structures.
"""
import spglib
import numpy as np

from pathlib import Path
from copy import deepcopy

from crysp.tasks.general import (gpaw, get_mindist_kpoints)
from ase import Atoms
from ase.build import niggli_reduce
from ase.optimize import BFGS
from ase.constraints import ExpCellFilter
from ase.calculators.calculator import kpts2kpts
from ase.dft.kpoints import mindistance2monkhorstpack


# structure manipulation tasks - should be moved into ase -- this needs to be
# in ase under the atoms object class
def reduce_to_primitive(atoms: Atoms) -> Atoms:
    """
    Helper function to reduce the structure to the smallest unit cell possible
    i.e. reduce supercells. Removes atom properties.

    :param atoms: ASE atoms object to find primitive lattice for.
    :return: reduced atoms object
    """
    cell, spos, numbers = spglib.find_primitive(
        cell=(atoms.cell, atoms.get_scaled_positions(), atoms.numbers))

    return Atoms(scaled_positions=spos, numbers=numbers, cell=cell, pbc=True)


def niggli_normalize(atoms: Atoms) -> Atoms:
    """
    Helper function that writes a niggli.traj file for a structure and
    returns the path object for the traj file.

    :param atoms: ASE atoms object to be standardized using a niggli reduction.
    :return: path object for trajectory of niggli reduced atoms.
    """
    atoms = atoms.copy()
    niggli_reduce(atoms)
    path = Path('niggli.traj')
    atoms.write(path)
    return atoms


# set-up or check magnetic props: I think this should be in ase under some
# magnetization subclass of atoms.
def check_magstate(calc, atomic_mom_threshold: float = 0.1,
                   total_mom_threshold: float = 0.1) -> str:
    """
    Check the atoms object for magnetic moment information. If the atoms
    object has magnetic information, first atoms are checked if any values is
    above atomic_mom_threshold, then check for afm, then fm. Returns unknown
    if no magnetic information is found.

    :param calc: calculator object to check magnetic moments of atoms.
    :param atomic_mom_threshold: Atomic magnetic moment threshold.
    :param total_mom_threshold: total magnetic moment threshold.

    :return: str indicating if the structure is magnetic (fm or afm) or
    non-magnetic (nm)
    """
    # get magmoms from calculation
    magmoms = calc.get_property('magmoms', allow_calculation=False)

    # if no magmoms == unknown
    if magmoms is None:
        return 'unknown'

    # find any atoms with magmom above threshold
    maximum_mom = abs(magmoms).max()
    if maximum_mom < atomic_mom_threshold:
        return 'nm'

    # test if there is afm ordering if magmoms above threshold
    magmom = abs(calc.get_magnetic_moment())
    if magmom < total_mom_threshold and maximum_mom > atomic_mom_threshold:
        return 'afm'
    return 'fm'


def magnetize(atoms: Atoms, magmom: float = 1.0) -> Atoms:
    """
    Helper function to quickly set the magnetic moments for atoms objects.
    Defaults to 1.0 but can provide an alternative value.

    :param atoms: atoms object
    :param magmom: Value to initialize the magnetic moments of all atoms.
        Defaults to 1.0; chose any value.
    :return: atoms object with initial magnetic moments set
    """
    atoms.set_initial_magnetic_moments([magmom] * len(atoms))
    return atoms


def relax3d(atoms: Atoms, calculator: dict) -> dict:
    """
    Perform a 3D structure relaxation using GPAW. Spins are handled in the
    usual way i.e. by GPAW. Specifying smax and fmax in the calculator
    dictionary will set stress and force tolerances, respectively.

    Currently, after structure optimization, the workflow checks the system's
    magnetization. If none are above the threshold, the calculation is re-run
    as spin paired and returned. This will be moved once dynamic WFs exist.

    :param atoms: Atoms object to relax.
    :param calculator: A dictionary containing the calculator input parameters.
        If the stress 'smax' and force 'convergence.forces' tolerances are
        not provided a value of 0.005 and 0.05 is assumed.
    :return: dict of {'atoms': Atoms object (relaxed), 'traj_path': Path
    object}
    """
    # pull default cut-offs from calculator; set if there are none
    smax = calculator.pop('smax', 0.005)
    fmax = calculator['convergence'].get('forces', 0.05) \
        if calculator.get('convergence', False) else 0.05

    calculator['kpts'] = kpts2kpts(calculator['kpts'], atoms) if isinstance(
        calculator['kpts'], dict) else calculator['kpts']  # kpts obj

    nm = False # OBS: This one is needed to check if we shoud initialize a 
    # new parallel context with calc_nm after the initial relaxation.
    
    # Construct & run optimizer
    with gpaw(calculator) as atoms.calc:
        atoms, traj = run_optimization(atoms, fmax=fmax, smax=smax,
                                       identifier='relax')

        # check magnetic state; re-run if nm & spinpol!=False: TODO this is a wf
        #  level decision and should be something the user should know about
        if check_magstate(atoms.calc) == 'nm' and atoms.calc.get_spin_polarized() \
                is not False:
            # set 0 on atoms to run nm calc
            atoms = magnetize(atoms=atoms, magmom=0.0)
            # re-initialize calculator and update txt log file name
            calc_nm = deepcopy(calculator)
            calc_nm.update({'txt': "relax-nm.txt"})
            nm = True # OBS: This one is needed to check if we shoud initialize a 
            # new parallel context with calc_nm after the initial relaxation.

    if nm: 
        # This is not the most elegant solution, but we need to initialize a 
        # a new parallel context with calc_nm and we cannot run check_magstate
        # on the original calculator if we break the original context. 
        with gpaw(calc_nm) as atoms.calc:
            # We consider this an extension of the same relaxation, so we pass
            # the same identifier and hence reuse the same files.
            atoms, traj = run_optimization(atoms, fmax=fmax, smax=smax,
                                               identifier='relax-nm')

    return {'atoms': atoms.copy(), 'traj_path': traj}
# TODO: separate check magnetic_state into a different tasks. leaf spawn node
# TODO: separate out non-magnetic part of the workflow
# TODO: write test for check_magstate, get_mindist_kpoints,
#  and reduce_to_primitive


def run_optimization(atoms: Atoms, fmax: float, smax: float,
                     smask=np.array([1, 1, 1, 1, 1, 1]),
                     *,
                     identifier: str): # -> typing.Tuple[Atoms, Path]:
    """
    Use GPAW with BGFS ASE optimizer to perform structure optimization using
    custom force and stress convergence criteria. GPAW sys.out prefix from
    the calculator object is used to set all output files.

    :param atoms: Atoms object with a calculator object attached to the
        structure.
    :param fmax: Force cut off.
    :param smax: Stress tensor cut off.
    :param smask: 1x6 np.array to select the diagonal components of the
        stress tensor. 0=ignored, 1=checked.
    :param identifier: Name for the restart file, typically use restart.json.
    :return: Atoms object (relaxed), Path('traj')
    """
    # restart/checkpoint - if tmp_atoms file exists restart
    # if len(glob.glob(tmp_atoms)) != 0:
    #     from ase.io.trajectory import Trajectory
    #     try:
    #         # Trying to avoid weird error upon submission
    #         with Trajectory(tmp_atoms, 'r') as t:
    #             atoms = t[-1]
    #         print('reading traj file')
    #     except InvalidULMFileError:
    #         tmp_atoms = None
    # else:
    #     tmp_atoms = None
    # we take prefix for files from txt file override default log file for our
    # own that doesn't have errors writing the max stresses and forces

    trajectory = f'{identifier}.traj'
    logfile = f'{identifier}.log'
    with BFGS(ExpCellFilter(atoms), trajectory=trajectory,
              logfile=None, restart=f'restart.{identifier}.json') as opt, \
            open(logfile, 'a') as log_fd:
        # we hardcode the restart file because doing both mag and non mag
        # runs, we expect them to use the same hessian.
        print('{:12s} {:17s} {:18s} {:16s} {:6s}'.format(
            'Step', 'Time', 'Energy', 'fmax', 'smax'), file=log_fd)

        # set the force cut off for conv; apply custom conv criteria with irun
        for i, _ in enumerate(opt.irun(fmax=0)):
            # params to write out to log
            f = atoms.get_forces()
            fmaxnow = np.linalg.norm(f, axis=1).max()**0.5
            s = atoms.get_stress() * smask
            smaxnow = abs(s).max()
            e = atoms.get_potential_energy(force_consistent=True)

            # time stamp
            from datetime import datetime
            now = str(datetime.now()).rsplit('.', 1)[0]  # rm frac secs

            # loop break condition
            done = fmaxnow <= fmax and smaxnow <= smax

            print(f'{i:4d} {now} {e:16.6f} {fmaxnow:16.6f} {smaxnow:16.6f}',
                  file=log_fd)
            if done:
                opt.call_observers()
                break
    return atoms, Path(trajectory)
    # TODO figure a better way to get the traj file name or return the name
    #  for some file that can be used to process the calculation

