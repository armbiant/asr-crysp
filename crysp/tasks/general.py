"""
General tasks associated with high-throughput calculations of materials.
Intended use for 3D structures.
"""
from contextlib import contextmanager

import numpy as np

from ase import Atoms
from ase.dft.kpoints import mindistance2monkhorstpack
from ase.calculators.subprocesscalculator import (
    NamedPackedCalculator, MPICommand, PythonSubProcessCalculator)


def dummy_parallel_function(a, b):
    from gpaw.mpi import world
    print(f'hello world {world.rank}/{world.size}')
    return a + b, world.sum(world.rank)


def mpi_command():
    return MPICommand([
        'mpiexec',
        'gpaw', 'python', '-m', 'ase.calculators.subprocesscalculator',
        'standard'])


@contextmanager
def mpi():
    """Yield an object that can run functions inside MPI subprocess.

    with mpi() as parallel:
        parallel.call(myfunction, arg1, arg2, arg3=42)

    """
    from ase.calculators.subprocesscalculator import ParallelDispatch
    with ParallelDispatch(mpi_command()) as parallel:
        yield parallel


@contextmanager
def gpaw(calculator):
    """
    Parallel context for performing a GPAW calculations in a parallel
    environment. It can be thought of as a wrapper for the usual GPAW
    needed for ASR
    """

    pack = NamedPackedCalculator('gpaw', calculator)
    with SubProcessGPAW(pack, mpi_command()) as calc:
        yield calc

class SubProcessGPAW(PythonSubProcessCalculator):
    """
    Subclass which ensures that functionality such as
    write is properly forwarded from the 
    PythonSubProcessCalculator from ase
    """
    # The forward methods list should include all methods that
    # the SubProcessGPAW needs to inherit from the GPAW 
    # calculator class
    _forward_methods = {'write',
            'get_spin_polarized'}
    def __getattr__(self, attr):
        if attr in self._forward_methods:
            return getattr(self.backend(), attr)
        else:
            raise AttributeError('Some kind of forwarding error message {attr}')


def get_mindist_kpoints(atoms: Atoms, calculator: dict,  density: float = 4) -> list:
    """
    Helper function to get the k-point grid based on minimum distance between
    real space lattice points

    :param atoms: ASE atoms object to be standardized using a niggli reduction.
    :param calculator: A dictionary containing the calculator input parameters.
    :param density: The desired k-point density
    :return: list with number of k-points in the directions of the unit cell
    vectors
    """
    kpts = mindistance2monkhorstpack(
        atoms, min_distance = 2*np.pi*calculator['kpts']['density'],
        maxperdim=50)
    calculator['kpts'] = kpts

    return calculator


