import asr

@asr.workflow
class OQMDWorkflow:
    """

    """
    initial_atoms = asr.var()
    # Defining the calculator to be used for the relaxation
    @asr.define
    def calculator_relax(self):
        return {
            'kpts': {'density': 1.0,  'gamma': True},
            'mode': {'name': 'pw', 'ecut': 800}, 'xc': 'PBE', 'txt':
                'relax.txt', 'convergence': {'forces': 5e-2},
                'occupations': {'name': 'fermi-dirac', 'width': 0.05}}

    # Invoking the relax3d subworkflow
    @asr.subworkflow
    def relax(self):
        from crysp.workflows.relax3d import RelaxWorkflow
        return RelaxWorkflow(initial_atoms=self.initial_atoms,
                             calc=self.calculator_relax)

    # Defining the calculator to be used for the groundstate calculation
    @asr.define
    def calculator_groundstate(self):
        return {
        'mode': {'name': 'pw', 'ecut': 800},
        'xc': 'PBE', 'kpts': {'density': 2.0, 'gamma': True},
        'occupations': {'name': 'fermi-dirac','width': 0.05},
        'convergence': {'bands': 'CBM+3.0'},'nbands': '200%',
        'txt': 'gs.txt','charge': 0}

    # Invoking the gs subworkflow
    @asr.subworkflow
    def gs(self):
        from crysp.workflows.gs import GsWorkflow
        return GsWorkflow(atoms=self.relax.relax3d_magnetic['atoms'],
                          calc=self.calculator_groundstate)

    # Defining the bs settings used for the bs calculation
    @asr.define
    def bs_settings(self):
        return {'bandpath': None,
                'density': 1}

    # Invoking the bs subworkflow
    @asr.subworkflow
    def bs(self):
        from crysp.workflows.bs import BsWorkflow
        return BsWorkflow(gs_gpw_file_path=self.gs.groundstate,
                          bs_settings=self.bs_settings)


@asr.parametrize_glob('*/material')
def workflow(material):
    """

    :param material:
    :return:
    """
    return OQMDWorkflow(initial_atoms=material)
