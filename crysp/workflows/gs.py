"""
Workflow to perform electronic structure calculations for 3d materials.
Run this script to submit the workflow.
"""
import asr


@asr.workflow
class GsWorkflow:
    """

    """
    atoms = asr.var()
    calc = asr.var()

    # We need this one to make the get_mindist_kpoints to work
    @asr.define
    def calculator(self):
        return self.calc

    @asr.task
    def get_kpoints_gs(self):
        """
        Task used to determine the k-point grid to be used 
        for the groundstate calculation

        :return: k-point grid for the particular structure
        """
        return asr.node('crysp.tasks.general.get_mindist_kpoints',
                        calculator=self.calculator,
                        atoms=self.atoms)

    @asr.task
    def groundstate(self):
        """
        Task to calculate the ground state

        :return: calculator and gs.gpw file
        """
        return asr.node('crysp.tasks.gs.gs_calculate',
                        atoms=self.atoms,
                        calculator=self.get_kpoints_gs)

    @asr.task
    def groundstate_postprocess(self):
        """
        Task to perform ground state post-processing

        :return: dict with basic gs information
        """
        # This should eventually return a gs results object
        return asr.node('crysp.postprocess.gs.postprocess',
                        groundstate=self.groundstate)


# Groundstate workflow function
@asr.parametrize_glob('*/material')
def workflow(material):
    """

    :param material:
    :return:
    """
    calculator = {'name': 'gpaw',
                  'mode': {'name': 'pw', 'ecut': 800}, 'xc': 'PBE',
                  'kpts': {'density': 1, 'gamma': True},
                  'convergence': {'forces': 5e-2},
                  'txt': 'gs.txt',
                  'occupations': {'name': 'fermi-dirac', 'width': 0.05}
                  }
    return GsWorkflow(atoms=material, calc=calculator)
