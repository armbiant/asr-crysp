import asr

@asr.workflow
class GsBsWorkflow:
    initial_atoms = asr.var()

    # Defining the calculator to be used for the groundstate calculation
    @asr.define
    def calculator_groundstate(self):
        return {
        'mode': {'name': 'pw', 'ecut': 200},
        'xc': 'PBE', 'kpts': {'density': 1.0, 'gamma': True},
        'occupations': {'name': 'fermi-dirac','width': 0.05},
        'convergence': {'bands': 'CBM+3.0'},'nbands': '200%',
        'txt': 'gs.txt','charge': 0}

    # Invoking the gs subworkflow
    @asr.subworkflow
    def gs(self):
        from crysp.workflows.gs import GsWorkflow
        return GsWorkflow(atoms=self.initial_atoms,
                          calc=self.calculator_groundstate)

    # Defining the bs settings used for the bs calculation
    @asr.define
    def bs_settings(self):
        return {'bandpath': None,
                'density': 1}

    # Invoking the bs workflow
    @asr.subworkflow
    def bs(self):
        from crysp.workflows.bs import BsWorkflow
        return BsWorkflow(gs_gpw_file_path=self.gs.groundstate,
                          bs_settings=self.bs_settings)

@asr.parametrize_glob('*/material')
def workflow(material):
    return GsBsWorkflow(initial_atoms=material)
