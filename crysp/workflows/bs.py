
"""
Workflow to perform band structure calculations for 3d materials.
Run this script to submit the workflow

----------------
Notes, comments, important information:

"""
import asr


@asr.workflow
class BsWorkflow:
    gs_gpw_file_path = asr.var()
    bs_settings = asr.var()

    @asr.task
    def bandstructure(self):
        """
        Task to perform a band structure calculation
        :return: path to bandstructure.json
        """
        return asr.node('crysp.tasks.bs.bs_calculate',
                        gs_gpw_file_path=self.gs_gpw_file_path,
                        bs_settings=self.bs_settings)
