"""
Workflow to perform full 3d structure optimization using GPAW. Run this
script to submit the workflow.

--------------------
Notes, comments, important information:

Development in form anaconda environment

VERSION DEV.
------------
ASR - '0.4.1' git branch htw-util-compat
ASE - '3.23.0b1'
HTWUTIL - master branch
GPAW - '22.8.1b1'


"""
import asr


@asr.workflow
class RelaxWorkflow:
    """

    """
    initial_atoms = asr.var()
    calc = asr.var()

    @asr.define
    def calculator(self):
        return self.calc

    @asr.task
    def reduce_to_primitive(self):
        """
        Task used to reduced a cell to a primitive lattice.

        :return: asr.node
        """
        return asr.node('crysp.tasks.relax.reduce_to_primitive',
                        atoms=self.initial_atoms)

    @asr.task
    def niggli_normalize(self):
        """
        Task used to obtain a standardized structure format.

        :return: path object pointing to the niggli normalized file
        """
        return asr.node('crysp.tasks.relax.niggli_normalize',
                        atoms=self.reduce_to_primitive)

    @asr.task
    def get_kpoints(self):
        """
        Task used to determine the k-point grid to be used in the relaxation

        :return: k-point grid for the particular structure
        """
        return asr.node('crysp.tasks.general.get_mindist_kpoints',
                        calculator=self.calculator,
                        atoms=self.niggli_normalize)

    @asr.task
    def magnetize(self):
        """
        Sets a large initial magnetic moment on the atoms so the first
        calculation is always spin polarized.

        :return: atoms object
        """
        return asr.node('crysp.tasks.relax.magnetize',
                        atoms=self.niggli_normalize)

    @asr.task
    def relax3d_magnetic(self):
        """
        Perform a single structure relaxation given smax and fmax (
        convergence.forces) using BFGS optimizer.

        :return: atoms, traj path object
        """
        return asr.node('crysp.tasks.relax.relax3d',
                        atoms=self.magnetize,
                        calculator=self.get_kpoints)

    # @asr.task
    # def check_magnetization(self):
    #     return asr.node('crysp.tasks.relax.check_magstate',
    #                     calc=self.relax3d['atoms'].calc)

    @asr.task
    def postprocess(self):
        """
        Custom post-processing routine for relaxation workflow

        :return:
        """
        return asr.node('crysp.postprocess.relax.postprocess',
                        relax=self.relax3d_magnetic)


# Relaxation workflow function
@asr.parametrize_glob('*/material')
def workflow(material):
    """

    :param material:
    :return:
    """
    calculator = {'mode': {'name': 'pw', 'ecut': 800}, 'xc': 'PBE',
                  'kpts': {'density': 1, 'gamma': True},
                  'convergence': {'forces': 5e-2}, 'smax': 5e-3,
                  'txt': 'relax.txt',
                  'occupations': {'name': 'fermi-dirac', 'width': 0.05}
                  }
    return RelaxWorkflow(initial_atoms=material, calc=calculator)
