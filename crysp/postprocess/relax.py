"""
Collection of post-processing scripts for the relaxation workflow to assist in
the analysis of relaxation workflows.
"""


def postprocess(relax):
    """
    Post process the output from a relaxation workflow.

    :param relax: object returned by the relax3d task.
    :return: dictionary representation of the calculation results
    """
    from ase.io import read
    traj = read(relax['traj_path'])
    atoms = relax['atoms']

    # Now that some checks are done, we can extract information
    forces = traj.calc.get_property('forces', allow_calculation=False)
    stresses = traj.calc.get_property('stress', allow_calculation=False)
    etot = traj.get_potential_energy()
    epa = etot / len(atoms)

    return dict(forces=forces, stresses=stresses, etot=etot, epa=epa,
                atoms=atoms)
