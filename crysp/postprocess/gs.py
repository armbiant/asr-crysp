"""
Postprocessing steps for the ground state workflow
"""
import json
from pathlib import Path
from gpaw import GPAW
from ase.dft.bandgap import bandgap

def postprocess(groundstate: Path) -> dict:
    """
    Post process the output from the gs workflow
    """
    # Taking the output of the groundstate as input
    calculation = GPAW(groundstate)

    # Some checks that the calculation are done
    forces = calculation.get_property('forces', allow_calculation=False)
    stresses = calculation.get_property('stress', allow_calculation=False)
    etot = calculation.get_potential_energy()

    # Reading the relevant properties
    efermi = calculation.get_fermi_level()
    gap, _, _ = bandgap(calculation)
    gap_dir, _, _ = bandgap(calculation, direct=True)

    gsresult = dict(efermi=efermi, gap=gap, gap_dir=gap_dir)

    # Writing simple results file 
    # Very much NOT parallel safe! Depending on worker design
    # this needs to be fixed
    with open('gsresult.json', 'w') as filename:
            json.dump(gsresult, filename)

    return gsresult
